# Policies App

## Supported versions:

- Java 8 to 13
- Spring boot 2.2.0.RELEASE
- MongoDB 4.4.0
- MongoDB Java driver 3.8.2
- Maven 3.6.3

## Requirements

You will need Java (at least Java 8) and latest MongoDB installed ( and running on your system ) in order to use this repo. 

This was only tested on a Mac machine. Also latest version of Maven is required.

## Automatic Setup

This is only for OSX user. 

Run the provided <strong>osx_setup.sh</strong> script.

This should install all the required (Maven, Java, MongoDB)

It will also start MongodDB and the Spring Boot application itself!

Once started, head over to the Swagger page: [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)

Follow the manual steps below if the Automatic setup does not work. 


## Manual Setup

If the previous Script did not work for your machine and/or you are not runnign on OSX, the follow the manual steps below

### Install Java 8 
if you dont already have it. 

### Install Mongodb.
On Mac use Brew, see https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/

TL/DR:
- brew tap mongodb/brew
- brew install mongodb-community@4.4

That should get Mongo installed


### Run Mongo DB server
- sudo mongod --dbpath </your/dbs/folder> --bind_ip 127.0.0.1

Optionally you may also run as a brew service. 

Prerequisite: 
Create the folder </your/dbs/folder> before attempting to start mongod. 


### Clone the repo 

Use: git clone https://mattia515@bitbucket.org/tiklainc/policyplay.git


## Commands

- Start the server in a console with `mvn spring-boot:run`.
- If you add some Unit Tests, you would start them with `mvn clean test`.
- You can start the end to end tests with `mvn clean integration-test`.
- You can build the project with : `mvn clean package`.
- You can run the project with the fat jar and the embedded Tomcat: `java -jar target/insurance-policies-1.0.0.jar` but I would use a real tomcat in production.

## Swagger
- Swagger is already configured in this project in `SwaggerConfig.java`.
- The API can be seen at [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html).
- You can also try the entire REST API directly from the Swagger interface!


## Author
- MP
