#!/usr/bin/env bash
# 
# Bootstrap script for setting up a new OSX machine with: 
# - brew (homebrew)
# - java (java8)
# - maven
# - mongodb-community edition
# 
# This should be idempotent so it can be run multiple times.
#
# Notes:
#

echo "Starting bootstrapping"

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
    echo "Installing homebrew..."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi


# Folder (under user's home) used by Mongo for its DB/service (optionally change this)
# This folder will be created by the script
MONGO_DATA_DIRECTORY=~/mongo_data
MONGO_DB_DIRECTORY=$MONGO_DATA_DIRECTORY/db 

MONGO_BIND_IP_TO_HOST=127.0.0.1


# Update homebrew recipes
brew update


# Install Maven (if not already there)
echo "Installing Maven..."
brew install maven

# Configure profile file for Maven options??? ie  such as: 
# export M2_HOME=/usr/local/Cellar/maven/3.6.3_1/libexec
#export M2=${M2_HOME}/bin
#export PATH=${PATH}:${M2_HOME}/bin
#[ -d $M2_HOME ] && echo "    Directory $M2_HOME exists." || echo "    Error: Directory $M2_HOME does not exists."

# Install Java 11 (if not already there)
echo "Installing Java 11..." 
brew tap adoptopenjdk/openjdk
brew cask install adoptopenjdk11

#Install Mongo DB (if not already there)
echo "Installing MongoDb Community edition..."
brew tap mongodb/brew
brew install mongodb-community@4.4

# Create a folder for MongoDB under the current users's Home folder in folder "data/db" 
rm -rf $MONGO_DATA_DIRECTORY/$MONGO_DB_DIRECTORY
mkdir $MONGO_DATA_DIRECTORY
mkdir $MONGO_DB_DIRECTORY

#Start MongoDB server (starts mongod directly and binds to bind_ip 
# Make sure no other MongoDB processes are running (kill any)
sudo killall mongod

# Startup Mongod
echo "Starting Mongod Server"
sudo mongod --dbpath $MONGO_DATA_DIRECTORY --bind_ip $MONGO_BIND_IP_TO_HOST & 


echo "Cleaning up..."
brew untap adoptopenjdk/openjdk
brew untap caskroom/versions
brew cleanup

echo "Bootstrapping complete"
echo "Compile and Start the App"

# Compile + Package app
mvn clean package

#Start up the RESTFul Spring Boot app
mvn spring-boot:run

