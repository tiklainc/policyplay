package com.mati.insu.policies;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import javax.annotation.PostConstruct;

import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.mati.insu.policies.controllers.UserController;
import com.mati.insu.policies.models.User;
import com.mati.insu.policies.repositories.UserRepository;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate rest;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private UserController userController;

	@Autowired
	private TestHelper testHelper;
	private String URL;

	@Autowired
	UserControllerIT(MongoClient mongoClient) {
		createUserCollectionIfNotPresent(mongoClient);
	}

	@PostConstruct
	void setUp() {
		URL = "http://localhost:" + port + "/api";
	}

	@AfterEach
	void tearDown() {
		// userRepository.deleteAll();
	}

	@DisplayName("GET /user/{id}")
	@Test
	void getUserById() {
		// GIVEN
		User userInserted = userRepository.save(testHelper.getOtherTestUser());
		ObjectId idInserted = userInserted.getId();
		// WHEN
		ResponseEntity<User> result = rest.getForEntity(URL + "/user/" + idInserted, User.class);
		// THEN
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody().getFirstName()).isEqualTo(userInserted.getFirstName());
		assertThat(result.getBody().getLastName()).isEqualTo(userInserted.getLastName());
	}

	@DisplayName("DELETE /user/{id}")
	@Test
	void deleteUserById() {
		// GIVEN
		User userInserted = userRepository.save(testHelper.getTestUser());
		ObjectId idInserted = userInserted.getId();
		// WHEN
		ResponseEntity<Long> result = rest.exchange(URL + "/user/" + idInserted.toString(), HttpMethod.DELETE, null,
				new ParameterizedTypeReference<Long>() {
				});
		// THEN
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody()).isEqualTo(1L);

	}

	@DisplayName("PUT /user")
	@Test
	void putUser() {
		// GIVEN
		User userInserted = userRepository.save(testHelper.getTestUser());
		// WHEN
		HttpEntity<User> body = new HttpEntity<>(userInserted);
		ResponseEntity<User> result = rest.exchange(URL + "/user", HttpMethod.PUT, body,
				new ParameterizedTypeReference<User>() {
				});
		// THEN
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

	}

	@DisplayName("POST /user")
	@Test
	void postUserViaCtrl() {

		User userReq = testHelper.getTestUser();

		User userInserted = userController.postUser(userReq);

		assertThat(userInserted.getFirstName().equalsIgnoreCase(userReq.getFirstName()));

		assertThat(userInserted.getLastName().equalsIgnoreCase(userReq.getLastName()));

		assertThat(userInserted.getId() != null);

		User check = userRepository.findOne(userInserted.getId().toString());

		assertThat(check.getId().toString().equalsIgnoreCase(userInserted.getId().toString()));
	}

	private void createUserCollectionIfNotPresent(MongoClient mongoClient) {
		// This is required because it is not possible to create a new collection within
		// a multi-documents transaction.
		// Some tests start by inserting 2 documents with a transaction.
		MongoDatabase db = mongoClient.getDatabase("test");
		if (!db.listCollectionNames().into(new ArrayList<>()).contains("users")) {
			db.createCollection("users");
		}
	}

}
