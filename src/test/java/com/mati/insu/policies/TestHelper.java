package com.mati.insu.policies;

import com.mati.insu.policies.enums.CoverType;
import com.mati.insu.policies.enums.PolicyType;
import com.mati.insu.policies.models.Asset;
import com.mati.insu.policies.models.Policy;
import com.mati.insu.policies.models.PolicyDetails;
import com.mati.insu.policies.models.PolicyRequest;
import com.mati.insu.policies.models.User;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Component;
import java.util.ArrayList;

@Component
class TestHelper {

	User getTestUser() {
		return new User().setFirstName("Mati").setLastName("Matatu").setEmailAddress("email@address.com")
				.setIdNumber("666666666");

	}

	User getOtherTestUser() {
		return new User().setFirstName("Pablo").setLastName("Pablito");
	}

	Asset getTestAsset() {
		return new Asset().setMake("Ferrari").setMaxSpeedKmH(330f).setModel("2017").setName("Ferrari Testa Rossa")
				.setDescription("Un lindo ferrari");
	}

	Policy getMaxPolicy() {

		ArrayList<CoverType> coverTypes = new ArrayList<CoverType>();
		coverTypes.add(CoverType.DM);
		coverTypes.add(CoverType.DT);

		return new Policy().setName("Mati Policy").setSuspended(false)
				.setPolicyDetails(new PolicyDetails().setPolicyType(PolicyType.EST).setCoverTypes(coverTypes))
				.setUserId("5f3f24deb65e23cc65457fff");

	}

	PolicyRequest getPolicyCreateRequest_HappyPath() {

		Asset car = new Asset().setName("Toyota");

		ArrayList<CoverType> coverTypes = new ArrayList<CoverType>();
		coverTypes.add(CoverType.DM);
		coverTypes.add(CoverType.DT);

		PolicyRequest req = new PolicyRequest()
				.setPolicyDetails(new PolicyDetails().setPolicyType(PolicyType.EST).setCoverTypes(coverTypes))
				.setAsset(car).setUser(getTestUser());

		return req;
	}

	PolicyRequest getPolicyCreateRequest_IDs_Path() {

		ArrayList<CoverType> coverTypes = new ArrayList<CoverType>();
		coverTypes.add(CoverType.DM);
		coverTypes.add(CoverType.DT);

		ObjectId userId = new ObjectId("5f41644cb65e23ed9abf41fc");

		ObjectId assetId = new ObjectId("5f416492b65e23ed9abf4201");

		PolicyRequest req = new PolicyRequest()
				.setPolicyDetails(new PolicyDetails().setPolicyType(PolicyType.EST).setCoverTypes(coverTypes))
				.setAsset(new Asset().setId(assetId)).setUser(new User().setId(userId));

		return req;
	}

}
