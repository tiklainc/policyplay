package com.mati.insu.policies;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.mati.insu.policies.controllers.AssetController;
import com.mati.insu.policies.models.Asset;
import com.mati.insu.policies.repositories.AssetRepository;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AssetControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private TestRestTemplate rest;

	@Autowired
	private AssetRepository assetRepository;

	@Autowired
	private AssetController assetController;

	@Autowired
	private TestHelper testHelper;
	private String URL;

	@Autowired
	AssetControllerIT(MongoClient mongoClient) {
		createAssetCollectionIfNotPresent(mongoClient);
	}

	@PostConstruct
	void setUp() {
		URL = "http://localhost:" + port + "/api";
	}

	@AfterEach
	void tearDown() {
		// assetRepository.deleteAll();
	}

	@DisplayName("GET /asset/{id}")
	@Test
	void gettAssetById() {
		// GIVEN
		Asset assetInserted = assetRepository.save(testHelper.getTestAsset());
		ObjectId idInserted = assetInserted.getId();
		// WHEN
		ResponseEntity<Asset> result = rest.getForEntity(URL + "/asset/" + idInserted, Asset.class);
		// THEN
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
		assertThat(result.getBody().getMake()).isEqualTo(assetInserted.getMake());
		assertThat(result.getBody().getModel()).isEqualTo(assetInserted.getModel());
	}

	@DisplayName("DELETE /asset/{id}")
	@Test
	void deletetAssetById() {
		// GIVEN
		Asset assetInserted = assetRepository.save(testHelper.getTestAsset());
		ObjectId idInserted = assetInserted.getId();
		// WHEN
		ResponseEntity<Long> result = rest.exchange(URL + "/user/" + idInserted.toString(), HttpMethod.DELETE, null,
				new ParameterizedTypeReference<Long>() {
				});
		// THEN
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

	}

	@DisplayName("PUT /asset")
	@Test
	void putAsset() {
		// GIVEN
		Asset assetInserted = assetRepository.save(testHelper.getTestAsset());
		// WHEN
		HttpEntity<Asset> body = new HttpEntity<>(assetInserted);
		ResponseEntity<Asset> result = rest.exchange(URL + "/user", HttpMethod.PUT, body,
				new ParameterizedTypeReference<Asset>() {
				});
		// THEN
		assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);

	}

	@DisplayName("POST /asset")
	@Test
	void posttAssetViaCtrl() {

		Asset assetReq = testHelper.getTestAsset();

		Asset assetInserted = assetController.postAsset(assetReq);

		// assertThat(assetInserted.getFirstName().equalsIgnoreCase(assetReq.getFirstName()));

		// assertThat(assetInserted.getLastName().equalsIgnoreCase(assetReq.getLastName()));

		assertThat(assetInserted.getId() != null);

		Asset check = assetRepository.findOne(assetInserted.getId().toString());

		assertThat(check.getId().toString().equalsIgnoreCase(assetInserted.getId().toString()));
	}

	private void createAssetCollectionIfNotPresent(MongoClient mongoClient) {
		// This is required because it is not possible to create a new collection within
		// a multi-documents transaction.
		// Some tests start by inserting 2 documents with a transaction.
		MongoDatabase db = mongoClient.getDatabase("test");
		if (!db.listCollectionNames().into(new ArrayList<>()).contains("assets")) {
			db.createCollection("assets");
		}
	}

}
