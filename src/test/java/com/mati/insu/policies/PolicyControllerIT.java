package com.mati.insu.policies;

import com.mati.insu.policies.controllers.PolicyController;
import com.mati.insu.policies.models.Policy;
import com.mati.insu.policies.models.PolicyRequest;
import com.mati.insu.policies.repositories.PolicyRepository;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class PolicyControllerIT {

	@LocalServerPort
	private int port;

	@Autowired
	private PolicyRepository policyRepository;

	@Autowired
	private PolicyController policyController;

	@Autowired
	private TestHelper testHelper;

	@Autowired
	PolicyControllerIT(MongoClient mongoClient) {

		createPolicyCollectionIfNotPresent(mongoClient);
	}

	@PostConstruct
	void setUp() {
	}

	@AfterEach
	void tearDown() {
		// userRepository.deleteAll();
		// TODO delete all for policies for testing
		// policyRepository.deleteAll();
	}

	@DisplayName("POST /policy")
	@Test
	void testCreatePolicy_HappyPath() {

		PolicyRequest req = testHelper.getPolicyCreateRequest_HappyPath();

		Policy policyInserted = policyController.createPolicy(req);

		assertThat(policyInserted.getPolicyDetails().getPolicyType()).isEqualTo(req.getPolicyDetails().getPolicyType());

		// assertThat(policyInserted.getAsset()).isEqualTo(req.getPolicyDetails().getCar());

		Policy check = policyRepository.findOne(policyInserted.getId().toString());

		assertThat(check.getId().toString().equalsIgnoreCase(policyInserted.getId().toString()));

	}

	@DisplayName("POST /policy")
	@Test
	void testCreatePolicy_IDS_Path() {

		PolicyRequest req = testHelper.getPolicyCreateRequest_IDs_Path();

		Policy policyInserted = policyController.createPolicy(req);

		assertThat(policyInserted.getPolicyDetails().getPolicyType()).isEqualTo(req.getPolicyDetails().getPolicyType());

		// assertThat(policyInserted.getAsset()).isEqualTo(req.getPolicyDetails().getCar());

		Policy check = policyRepository.findOne(policyInserted.getId().toString());

		assertThat(check.getId().toString().equalsIgnoreCase(policyInserted.getId().toString()));

	}

	/*
	 * @DisplayName("POST /policy/update/policy-details")
	 * 
	 * @Test void testCreatePolicy_Update_PolicyDetails() {
	 * 
	 * ArrayList<CoverType> coverTypes = new ArrayList<CoverType>();
	 * coverTypes.add(CoverType.DM); coverTypes.add(CoverType.DT);
	 * 
	 * PolicyDetails req = new PolicyDetails() .setPolicyType(PolicyType.EST)
	 * .setCoverTypes(coverTypes);
	 * 
	 * ObjectId policyId = new ObjectId("5f418e41b65e23f234411e39");
	 * 
	 * Policy policyInserted = policyController.updatePolicy(req,
	 * policyId.toString());
	 * 
	 * //assertThat(policyInserted.getPolicyDetails().getPolicyType()).isEqualTo(req
	 * .getPolicyDetails().getPolicyType());
	 * 
	 * //assertThat(policyInserted.getAsset()).isEqualTo(req.getPolicyDetails().
	 * getCar());
	 * 
	 * Policy check = policyRepository.findOne(policyInserted.getId().toString());
	 * 
	 * assertThat(check.getId().toString().equalsIgnoreCase(policyInserted.getId().
	 * toString()));
	 * 
	 * }
	 */

	/*
	 * @DisplayName("POST /policy/update/policy-details")
	 * 
	 * @Test void testCreatePolicy_Update_PolicyDetails_RCO() {
	 * 
	 * ArrayList<CoverType> coverTypes = new ArrayList<CoverType>();
	 * coverTypes.add(CoverType.DM); coverTypes.add(CoverType.DT);
	 * 
	 * PolicyDetails req = new PolicyDetails() .setPolicyType(PolicyType.RCO)
	 * .setCoverTypes(coverTypes);
	 * 
	 * ObjectId policyId = new ObjectId("5f418e41b65e23f234411e39");
	 * 
	 * Policy policyInserted = policyController.updatePolicy(req,
	 * policyId.toString());
	 * 
	 * //assertThat(policyInserted.getPolicyDetails().getPolicyType()).isEqualTo(req
	 * .getPolicyDetails().getPolicyType());
	 * 
	 * //assertThat(policyInserted.getAsset()).isEqualTo(req.getPolicyDetails().
	 * getCar());
	 * 
	 * Policy check = policyRepository.findOne(policyInserted.getId().toString());
	 * 
	 * assertThat(check.getId().toString().equalsIgnoreCase(policyInserted.getId().
	 * toString()));
	 * 
	 * }
	 */

	/*
	 * 
	 * THIS WAS WORKING
	 * 
	 * @DisplayName("PUT /policy")
	 * 
	 * @Test void testUpdatePolicy_UpdateCar() {
	 * 
	 * String newCarBrand = "Jaguar";
	 * 
	 * PolicyRequest req = testHelper.getPolicyCreateRequest(); Policy
	 * policyInserted = policyController.createPolicy(req); PolicyDetails
	 * policyDetails = policyInserted.getPolicyDetails();
	 * 
	 * assertThat(policyDetails.getCar().getBrand().equalsIgnoreCase(req.
	 * getPolicyDetails().getCar().getBrand()));
	 * assertThat(!policyDetails.getCar().getBrand().equalsIgnoreCase(newCarBrand));
	 * 
	 * String idPolicy = policyInserted.getId().toString();
	 * 
	 * // Change the Car Asset newCar = new Asset(); newCar.setBrand(newCarBrand);
	 * policyDetails.setCar(newCar); req.setPolicyDetails(policyDetails);
	 * 
	 * policyController.updatePolicy(req, idPolicy);
	 * 
	 * Policy check = policyRepository.findOne(idPolicy);
	 * assertThat(check.getId().toString().equalsIgnoreCase(policyInserted.getId().
	 * toString()));
	 * 
	 * }
	 */

	/*
	 * @DisplayName("PUT /policy")
	 * 
	 * @Test void testUpdatePolicy_UpdatePolicyTypeCoverage() {
	 * 
	 * 
	 * 
	 * PolicyRequest req = testHelper.getPolicyCreateRequest(); Policy
	 * policyInserted = policyController.createPolicy(req); PolicyDetails
	 * policyDetails = policyInserted.getPolicyDetails();
	 * 
	 * assertThat(policyDetails.getCar().getBrand().equalsIgnoreCase(req.
	 * getPolicyDetails().getCar().getBrand()));
	 * 
	 * 
	 * String idPolicy = policyInserted.getId().toString();
	 * 
	 * 
	 * 
	 * ArrayList<CoverType> covers = new ArrayList<CoverType>();
	 * 
	 * 
	 * // Change the Car Car newCar = new Car(); newCar.setBrand(newCarBrand);
	 * policyDetails.setCar(newCar); req.setPolicyDetails(policyDetails);
	 * 
	 * policyController.updatePolicy(req, idPolicy);
	 * 
	 * Policy check = policyRepository.findOne(idPolicy);
	 * assertThat(check.getId().toString().equalsIgnoreCase(policyInserted.getId().
	 * toString()));
	 * 
	 * 
	 * }
	 */

	private void createPolicyCollectionIfNotPresent(MongoClient mongoClient) {
		// This is required because it is not possible to create a new collection within
		// a multi-documents transaction.
		// Some tests start by inserting 2 documents with a transaction.
		MongoDatabase db = mongoClient.getDatabase("test");
		if (!db.listCollectionNames().into(new ArrayList<>()).contains("policies")) {
			db.createCollection("policies");
		}
	}
}
