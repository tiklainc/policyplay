package com.mati.insu.exception;

import java.io.Serializable;

public class CustomException extends RuntimeException implements Serializable {

	private static final long serialVersionUID = -1030647904692689603L;
	
	private final String error;
	
	public CustomException(String message) {
		
		super(message);
		this.error = message ; 
	}
}
