package com.mati.insu.validation;

import java.util.ArrayList;
import java.util.List;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.ControllerAdvice;
import com.google.common.collect.ImmutableList;
import com.mati.insu.policies.enums.CoverType;
import com.mati.insu.policies.enums.PolicyType;
import com.mati.insu.policies.models.Asset;
import com.mati.insu.policies.models.AssetReference;
import com.mati.insu.policies.models.PolicyDetails;
import com.mati.insu.policies.models.PolicyRequest;
import com.mati.insu.policies.models.User;
import com.mati.insu.policies.repositories.AssetRepository;
import com.mati.insu.policies.repositories.UserRepository;

@ControllerAdvice
public class ValidPolicyRequestValidator implements ConstraintValidator<ValidPolicyRequest, PolicyRequest> {
	
	private final static Logger LOGGER = LoggerFactory.getLogger(ValidPolicyRequestValidator.class);

	private final static String VALID_COVERAGE_RCO = "DTO";

	private final static List<String> VALID_POLICY_TYPES = ImmutableList.of("EST", "RCO");

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AssetRepository assetRepository;
	
    @Autowired
    private MessageSource messageSource;

	@Override
	public void initialize(ValidPolicyRequest validPolicyCoverage) {

	}

	@Override
	public boolean isValid(PolicyRequest policyRequest, ConstraintValidatorContext context) {

		// Validate User
		if (!validateUserDetails(policyRequest.getUser(), context)) {

			return false;
		}

		// Validate Asset (Assumes data for User was passed)
		if (!validateAssetDetails(policyRequest.getAsset(), policyRequest.getUser(), context)) {
			return false;
		}

		// Validate PolicyDetails
		if (!validatePolicyDetails(policyRequest.getPolicyDetails(), context)) {
			return false;
		}
		return true;
	}

	/**
	 * 
	 * Method to validate on allowed combinations of values for User/Asset
	 * 
	 * @param user
	 * @param asset
	 * @return
	 */
	private boolean validateAssetDetails(Asset asset, User user, ConstraintValidatorContext context) {

		// No Asset data provided
		if ((asset.getId() == null && noMandatoryAssetDataProvided(asset))) {
			context.buildConstraintViolationWithTemplate(
					messageSource.getMessage("error.val.policy.creation.bad.asset", null, null))
					.addConstraintViolation().disableDefaultConstraintViolation();
			return false;
		}

		// Providing an asset ID but not the ID of a User - not possible.
		if (asset.getId() != null && user.getId() == null) {
			context.buildConstraintViolationWithTemplate(
					messageSource.getMessage("error.val.policy.creation.new.user.existing.asset", new Object[] {asset.getId().toString()}, null))
					.addConstraintViolation()
					.disableDefaultConstraintViolation();
			return false;
		}

		// Providing an Asset ID AND a User ID - need to check the UserID + AssetID is valid...
		if (user.getId() != null && asset.getId() != null) {

			User uu = userRepository.findOne(user.getId().toString());
			boolean isExistingUserAsset = false;
			for (AssetReference aRef : uu.getAssetReferences()) {

				if (aRef.getAssetId().toString().equalsIgnoreCase(asset.getId().toString())) {
					isExistingUserAsset = true;
				}
			}
			if (!isExistingUserAsset) {
				context.buildConstraintViolationWithTemplate(
						messageSource.getMessage("error.val.policy.creation.bad.user.asset", new Object[] {asset.getId().toString(), user.getId().toString()}, null))
						.addConstraintViolation().disableDefaultConstraintViolation();
				return false;
			}
		}

		// Provided an asset
		if (asset.getId() != null && ObjectId.isValid(asset.getId().toString())) {
			Asset aa = assetRepository.findOne(asset.getId().toString());
			if (aa == null) {
				context.buildConstraintViolationWithTemplate(
						messageSource.getMessage("error.val.policy.creation.no.asset", new Object[] {asset.getId().toString()}, null))
						.addConstraintViolation().disableDefaultConstraintViolation();
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * Method for validating input User details
	 * 
	 * @param user
	 * @return
	 * @throws BadUserException
	 */
	private boolean validateUserDetails(User user, ConstraintValidatorContext context) {

		if ((user.getId() == null && noMandatoryUserDataProvided(user))) {
			
			context.buildConstraintViolationWithTemplate(
					messageSource.getMessage("error.val.policy.creation.bad.user", null, null))
					.addConstraintViolation().disableDefaultConstraintViolation();
			return false;
		}

		if (user.getId() != null) {

			User uu = userRepository.findOne(user.getId().toString());
			if (uu == null) {
				context.buildConstraintViolationWithTemplate(
						messageSource.getMessage("error.val.policy.creation.no.user", new Object[] {user.getId().toString()}, null))
						.addConstraintViolation().disableDefaultConstraintViolation();
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * Method for validating input PolicyDetails provided
	 * 
	 * @param policyDetails
	 * @return
	 */
	private boolean validatePolicyDetails(PolicyDetails policyDetails, ConstraintValidatorContext context) {

		PolicyType policyType = policyDetails.getPolicyType();
		ArrayList<CoverType> coverTypes = policyDetails.getCoverTypes();

		if (!VALID_POLICY_TYPES.contains(policyType.name())) {
			String errMessage = messageSource.getMessage("error.val.policy.creation.bad.type", new Object[] {policyType.name()}, null);
			context.buildConstraintViolationWithTemplate(errMessage)
					.addConstraintViolation().disableDefaultConstraintViolation();
			LOGGER.error(errMessage);
			return false;
		}

		// Check valid RCO policy & coverings!
		if (policyType == PolicyType.RCO) {
			if (coverTypes.size() != 1 || !VALID_COVERAGE_RCO.equalsIgnoreCase(coverTypes.get(0).name())) {
				
				String errMessage = messageSource.getMessage("error.val.policy.creation.bad.rco.cover", null, null);
				context.buildConstraintViolationWithTemplate(errMessage)
						.addConstraintViolation().disableDefaultConstraintViolation();
				LOGGER.error(errMessage);
				return false;
			}
		}

		// Check valid EST policy & coverings!
		if (policyType == PolicyType.EST) {
			// DT is mandatory 
			if (!coverTypes.contains(CoverType.DT)) {
				String errMessage = messageSource.getMessage("error.val.policy.creation.bad.est.no.dt", null, null);
				context.buildConstraintViolationWithTemplate(errMessage)
						.addConstraintViolation().disableDefaultConstraintViolation();
				LOGGER.error(errMessage);
				return false ; 
			}
		}
		return true;
	}
	
	
	// For Asset, only "name" is mandatory
	private boolean noMandatoryAssetDataProvided(Asset asset) {
		return (asset.getName() == null);
	}

	// For User, firstName, lastName and emailAddress are mandatory
	private boolean noMandatoryUserDataProvided(User user) {
		return (user.getFirstName() == null || user.getLastName() == null || user.getEmailAddress() == null
				|| user.getIdNumber() == null);
	}
}