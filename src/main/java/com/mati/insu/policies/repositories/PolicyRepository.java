package com.mati.insu.policies.repositories;

import org.springframework.stereotype.Repository;
import com.mati.insu.policies.models.Policy;

@Repository
public interface PolicyRepository {

	Policy save(Policy policy);

	Policy update(Policy policy);

	Policy findOne(String id);

}
