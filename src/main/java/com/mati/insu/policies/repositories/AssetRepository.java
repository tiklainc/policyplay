package com.mati.insu.policies.repositories;

import org.springframework.stereotype.Repository;
import com.mati.insu.policies.models.Asset;

@Repository
public interface AssetRepository {

	Asset save(Asset asset);

	Asset update(Asset asset);

	long delete(String id);

	Asset findOne(String id);

}
