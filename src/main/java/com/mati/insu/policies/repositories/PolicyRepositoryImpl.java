package com.mati.insu.policies.repositories;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.ReturnDocument.AFTER;
import javax.annotation.PostConstruct;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mati.insu.policies.SpringConfiguration;
import com.mati.insu.policies.models.Policy;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;

@Repository
public class PolicyRepositoryImpl implements PolicyRepository {

	private static final String POLICIES_COLLECTION = "policies";

	@Autowired
	private MongoClient client;
	private MongoCollection<Policy> policyCollection;

	@PostConstruct
	void init() {
		policyCollection = client.getDatabase(SpringConfiguration.MONGO_DB_DATABASE_NAME)
				.getCollection(POLICIES_COLLECTION, Policy.class);
	}

	@Override
	public Policy save(Policy policy) {
		policy.setId(new ObjectId());
		policyCollection.insertOne(policy);
		return policy;
	}

	@Override
	public Policy update(Policy policy) {
		FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().returnDocument(AFTER);
		return policyCollection.findOneAndReplace(eq("_id", policy.getId()), policy, options);
	}

	@Override
	public Policy findOne(String id) {
		return policyCollection.find(eq("_id", new ObjectId(id))).first();
	}

}
