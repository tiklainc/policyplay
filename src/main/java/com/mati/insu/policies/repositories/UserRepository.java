package com.mati.insu.policies.repositories;

import org.springframework.stereotype.Repository;
import com.mati.insu.policies.models.User;

@Repository
public interface UserRepository {

	User save(User user);

	User update(User user);

	long delete(String id);

	User findOne(String id);
	
    // long deleteAll();

}
