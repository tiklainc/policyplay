package com.mati.insu.policies.repositories;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.ReturnDocument.AFTER;
import javax.annotation.PostConstruct;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.mati.insu.policies.SpringConfiguration;
import com.mati.insu.policies.models.Asset;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;

@Repository
public class AssetRepositoryImpl implements AssetRepository {

	private static final String ASSETS_COLLECTION = "assets";
	@Autowired
	private MongoClient client;
	private MongoCollection<Asset> assetCollection;

	@PostConstruct
	void init() {
		assetCollection = client.getDatabase(SpringConfiguration.MONGO_DB_DATABASE_NAME)
				.getCollection(ASSETS_COLLECTION, Asset.class);
	}

	@Override
	public Asset save(Asset asset) {
		asset.setId(new ObjectId());
		assetCollection.insertOne(asset);
		return asset;
	}

	@Override
	public long delete(String id) {
		return assetCollection.deleteOne(eq("_id", new ObjectId(id))).getDeletedCount();
	}

	@Override
	public Asset update(Asset asset) {
		FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().returnDocument(AFTER);
		return assetCollection.findOneAndReplace(eq("_id", asset.getId()), asset, options);
	}

	@Override
	public Asset findOne(String id) {
		return assetCollection.find(eq("_id", new ObjectId(id))).first();
	}

}
