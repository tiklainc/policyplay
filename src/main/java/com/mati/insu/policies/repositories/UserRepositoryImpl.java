package com.mati.insu.policies.repositories;

import com.mati.insu.policies.SpringConfiguration;
import com.mati.insu.policies.models.User;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.FindOneAndReplaceOptions;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.annotation.PostConstruct;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.ReturnDocument.AFTER;

@Repository
public class UserRepositoryImpl implements UserRepository {

	/*
	  private static final TransactionOptions txnOptions =
	  TransactionOptions.builder() .readPreference(ReadPreference.primary())
	  .readConcern(ReadConcern.MAJORITY) .writeConcern(WriteConcern.MAJORITY)
	  .build();
	 */

	private static final String USERS_COLLECTION = "users";
	
	
	@Autowired
	private MongoClient client;
	private MongoCollection<User> userCollection;

	@PostConstruct
	void init() {
		userCollection = client
				.getDatabase(SpringConfiguration.MONGO_DB_DATABASE_NAME).getCollection(USERS_COLLECTION, User.class);
	}

	@Override
	public User save(User user) {
		user.setId(new ObjectId());
		userCollection.insertOne(user);
		return user;
	}

	@Override
	public long delete(String id) {
		return userCollection.deleteOne(eq("_id", new ObjectId(id))).getDeletedCount();
	}

	@Override
	public User update(User user) {
		FindOneAndReplaceOptions options = new FindOneAndReplaceOptions().returnDocument(AFTER);
		return userCollection.findOneAndReplace(eq("_id", user.getId()), user, options);
	}

	@Override
	public User findOne(String id) {
		return userCollection.find(eq("_id", new ObjectId(id))).first();
	}
	
	
	/*
    @Override
    public long deleteAll() {
        try (ClientSession clientSession = client.startSession()) {
            return clientSession.withTransaction(
                    () -> personCollection.deleteMany(clientSession, new BsonDocument()).getDeletedCount(), txnOptions);
        }

    }*/
}
