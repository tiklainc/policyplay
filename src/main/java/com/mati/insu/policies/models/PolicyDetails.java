package com.mati.insu.policies.models;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.mati.insu.policies.enums.CoverType;
import com.mati.insu.policies.enums.PolicyType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@JsonInclude(Include.NON_NULL)
public class PolicyDetails {

	@ApiModelProperty(position = 1, example = "EST")
	private PolicyType policyType;

	@ApiModelProperty(position = 2, example = "[DT, DM, RT]")
	private ArrayList<CoverType> coverTypes;

	public ArrayList<CoverType> getCoverTypes() {
		return coverTypes;
	}

	public PolicyDetails setCoverTypes(ArrayList<CoverType> coverTypes) {
		this.coverTypes = coverTypes;
		return this;
	}

	public PolicyType getPolicyType() {
		return policyType;
	}

	public PolicyDetails setPolicyType(PolicyType policyType) {
		this.policyType = policyType;
		return this;
	}

}
