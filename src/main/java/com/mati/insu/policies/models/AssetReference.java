package com.mati.insu.policies.models;

public class AssetReference {

	private String assetId;

	private String assetName;

	public String getAssetId() {
		return assetId;
	}

	public AssetReference setAssetId(String assetId) {
		this.assetId = assetId;
		return this;
	}

	public String getAssetName() {
		return assetName;
	}

	public AssetReference setAssetName(String assetName) {
		this.assetName = assetName;
		return this;
	}

}
