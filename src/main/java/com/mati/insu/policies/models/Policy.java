package com.mati.insu.policies.models;

import java.util.ArrayList;
import org.bson.types.ObjectId;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;

@ApiModel
@JsonInclude(Include.NON_NULL)
public class Policy {

	@JsonSerialize(using = ToStringSerializer.class)
	private ObjectId id;

	private PolicyDetails policyDetails;

	// The "calculated" Name for the Policy.
	private String name;

	// This is the User ID for which each policy is associatedv
	private String userId;

	// This is the Asset ID for which each policy is associated
	private String assetId;

	private ArrayList<PaymentDetails> billing;

	private boolean suspended;

	private ArrayList<ChangeLog> changeHistory;

	public ArrayList<ChangeLog> getChangeHistory() {
		return changeHistory;
	}

	public Policy setChangeHistory(ArrayList<ChangeLog> changeHistory) {
		this.changeHistory = changeHistory;
		return this;
	}

	public ObjectId getId() {
		return id;
	}

	public Policy setId(ObjectId id) {
		this.id = id;
		return this;
	}

	public String getUserId() {
		return userId;
	}

	public Policy setUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public String getName() {
		return name;
	}

	public Policy setName(String name) {
		this.name = name;
		return this;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public Policy setSuspended(boolean suspended) {
		this.suspended = suspended;
		return this;
	}

	public PolicyDetails getPolicyDetails() {
		return policyDetails;
	}

	public Policy setPolicyDetails(PolicyDetails policyDetails) {
		this.policyDetails = policyDetails;
		return this;
	}

	public String getAssetId() {
		return assetId;
	}

	public Policy setAssetId(String assetId) {
		this.assetId = assetId;
		return this;
	}

	public ArrayList<PaymentDetails> getBilling() {
		return billing;
	}

	public Policy setBilling(ArrayList<PaymentDetails> billing) {
		this.billing = billing;
		return this;
	}

}
