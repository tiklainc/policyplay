package com.mati.insu.policies.models;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ChangeLog {

	private LocalDate dateChanged;

	private String comment;

	public LocalDate getDateChanged() {
		return dateChanged;
	}

	public ChangeLog setDateChanged(LocalDate dateChanged) {
		this.dateChanged = dateChanged;
		return this;
	}

	public String getComment() {
		return comment;
	}

	public ChangeLog setComment(String comment) {
		this.comment = comment;
		return this;
	}

}
