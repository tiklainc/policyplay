package com.mati.insu.policies.models;

import java.util.ArrayList;
import org.bson.types.ObjectId;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel
@JsonInclude(Include.NON_NULL)
public class Asset {

	@ApiModelProperty(hidden = true)
	@JsonSerialize(using = ToStringSerializer.class)
	private ObjectId id;

	@ApiModelProperty(position = 1, example = "F40")
	private String name;

	@ApiModelProperty(position = 2, example = "Ferrari")
	private String make;

	@ApiModelProperty(position = 3, example = "2018")
	private String model;

	@ApiModelProperty(position = 4, example = "300")
	private Float maxSpeedKmH;

	@ApiModelProperty(position = 5, example = "Ferrari Testa Rossa")
	private String description;

	@ApiModelProperty(hidden = true)
	private ArrayList<PolicyReference> activePoliciesReferences;

	public ObjectId getId() {
		return id;
	}

	public Asset setId(ObjectId id) {
		this.id = id;
		return this;
	}

	public String getName() {
		return name;
	}

	public Asset setName(String brand) {
		this.name = brand;
		return this;
	}

	public ArrayList<PolicyReference> getActivePoliciesReferences() {
		return activePoliciesReferences;
	}

	public Asset setActivePoliciesReferences(ArrayList<PolicyReference> activePoliciesReferences) {
		this.activePoliciesReferences = activePoliciesReferences;
		return this;
	}

	public String getModel() {
		return model;
	}

	public Asset setModel(String model) {
		this.model = model;
		return this;
	}

	public Float getMaxSpeedKmH() {
		return maxSpeedKmH;
	}

	public Asset setMaxSpeedKmH(Float maxSpeedKmH) {
		this.maxSpeedKmH = maxSpeedKmH;
		return this;
	}

	public String getDescription() {
		return description;
	}

	public Asset setDescription(String description) {
		this.description = description;
		return this;
	}

	public String getMake() {
		return make;
	}

	public Asset setMake(String make) {
		this.make = make;
		return this;
	}

	// TODO - Nice to Have - @Overrides for toString() equals() and hashCode()
}
