package com.mati.insu.policies.models;

import com.mati.insu.policies.enums.PolicyType;

public class PolicyReference {

	private String policyId;

	private PolicyType policyType;

	public String getPolicyId() {
		return policyId;
	}

	public PolicyReference setPolicyId(String policyId) {
		this.policyId = policyId;
		return this;
	}

	public PolicyType getPolicyType() {
		return policyType;
	}

	public PolicyReference setPolicyType(PolicyType policyType) {
		this.policyType = policyType;
		return this;
	}

}
