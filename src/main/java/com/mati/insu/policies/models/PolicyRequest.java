package com.mati.insu.policies.models;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import io.swagger.annotations.ApiModel;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = false)
@ApiModel
public class PolicyRequest {

	@NotNull(message = "Policy details must be specified for creating a new Policy")
	private PolicyDetails policyDetails;

	@NotNull(message = "User details must be specified for creating a new Policy")
	private User user;

	@NotNull(message = "Asset details must be specified for creating a new Policy")
	private Asset asset;

	public Asset getAsset() {
		return asset;
	}

	public PolicyRequest setAsset(Asset asset) {
		this.asset = asset;
		return this;
	}

	public User getUser() {
		return user;
	}

	public PolicyDetails getPolicyDetails() {
		return policyDetails;
	}

	public PolicyRequest setPolicyDetails(PolicyDetails policyDetails) {
		this.policyDetails = policyDetails;
		return this;
	}

	public PolicyRequest setUser(User user) {
		this.user = user;
		return this;
	}

}
