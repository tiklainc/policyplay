package com.mati.insu.policies.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.bson.types.ObjectId;
import java.util.ArrayList;

@ApiModel
@JsonInclude(Include.NON_NULL)
public class User {

	@ApiModelProperty(hidden = true)
	@JsonSerialize(using = ToStringSerializer.class)
	private ObjectId id;

	@ApiModelProperty(position = 1, example = "Alice")
	private String firstName;

	@ApiModelProperty(position = 2, example = "Wonderland")
	private String lastName;

	@ApiModelProperty(position = 3, example = "alice@mail.com")
	private String emailAddress;

	@ApiModelProperty(position = 4, example = "00000000001a")
	private String idNumber;

	// List of active and inactive policy references
	@ApiModelProperty(hidden = true)
	private ArrayList<PolicyReference> policyReferences;

	// List of all user assets
	@ApiModelProperty(hidden = true)
	private ArrayList<AssetReference> assetReferences;

	public ArrayList<AssetReference> getAssetReferences() {
		return assetReferences;
	}

	public void setAssetReferences(ArrayList<AssetReference> assetReferences) {
		this.assetReferences = assetReferences;
	}

	public ArrayList<PolicyReference> getPolicyReferences() {
		return policyReferences;
	}

	public User setPolicyReferences(ArrayList<PolicyReference> policyReferences) {
		this.policyReferences = policyReferences;
		return this;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public User setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}

	public User setId(ObjectId id) {
		this.id = id;
		return this;
	}

	public ObjectId getId() {
		return id;
	}

	public String getFirstName() {
		return firstName;
	}

	public User setFirstName(String firstName) {
		this.firstName = firstName;
		return this;
	}

	public String getLastName() {
		return lastName;
	}

	public User setLastName(String lastName) {
		this.lastName = lastName;
		return this;
	}

	public String getIdNumber() {
		return idNumber;
	}

	public User setIdNumber(String idNumber) {
		this.idNumber = idNumber;
		return this;
	}

	/*
	 * @Override public String toString() { return "User{" + "id=" + id +
	 * ", firstName='" + firstName + '\'' + ", lastName='" + lastName + '\'' + '}';
	 * }
	 * 
	 * @Override public boolean equals(Object o) { if (this == o) return true; if (o
	 * == null || getClass() != o.getClass()) return false; User person = (User) o;
	 * return Objects.equals(id, person.id) && Objects.equals(firstName,
	 * person.firstName) && Objects.equals(lastName, person.lastName); }
	 * 
	 * @Override public int hashCode() { return Objects.hash(id, firstName,
	 * lastName); }
	 */

}