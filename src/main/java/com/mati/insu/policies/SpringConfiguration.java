package com.mati.insu.policies;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

@Configuration
public class SpringConfiguration {

	@Value("${spring.data.mongodb.uri}")
    private String connectionString;
     
	public static final String MONGO_DB_DATABASE_NAME = "test_database";
	
    private static final String MESSAGES_BUNDLE = "messages";

    @Bean
    public MongoClient mongoClient() {
    	CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), pojoCodecRegistry);
        return MongoClients.create(MongoClientSettings.builder()
    		   .applyConnectionString(new ConnectionString(connectionString))
               .codecRegistry(codecRegistry)
               .retryWrites(false)
               .build());
    }

    @Bean
    public ResourceBundleMessageSource messageSource() {
    	ResourceBundleMessageSource source = new ResourceBundleMessageSource();
        source.setBasenames(MESSAGES_BUNDLE);
        source.setUseCodeAsDefaultMessage(true);
        return source;
    }

}
