package com.mati.insu.policies.controllers;

import com.mati.insu.exception.CustomException;
import com.mati.insu.policies.enums.CoverType;
import com.mati.insu.policies.enums.PolicyType;
import com.mati.insu.policies.models.Asset;
import com.mati.insu.policies.models.AssetReference;
import com.mati.insu.policies.models.ChangeLog;
import com.mati.insu.policies.models.PaymentDetails;
import com.mati.insu.policies.models.Policy;
import com.mati.insu.policies.models.PolicyDetails;
import com.mati.insu.policies.models.PolicyReference;
import com.mati.insu.policies.models.PolicyRequest;
import com.mati.insu.policies.models.User;
import com.mati.insu.policies.repositories.AssetRepository;
import com.mati.insu.policies.repositories.PolicyRepository;
import com.mati.insu.policies.repositories.UserRepository;
import com.mati.insu.validation.ValidPolicyRequest;
import java.time.LocalDate;
import java.util.ArrayList;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@RestController
@RequestMapping("/api")
public class PolicyController {

	private final UserRepository userRepository;

	private final PolicyRepository policyRepository;

	private final AssetRepository assetRepository;
	
    @Autowired
    private MessageSource messageSource;

	public PolicyController(UserRepository userRepository, PolicyRepository policyRepository,
			AssetRepository assetRepository) {
		this.userRepository = userRepository;
		this.policyRepository = policyRepository;
		this.assetRepository = assetRepository;
	}

	/**
	 * 
	 * Controller method to retrieve a policy.
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("policy/{id}")
	public Policy getPolicy(@PathVariable String id) {
		Policy policy = loadPolicyIdOrFail(id);
		return policy;
	}

	/**
	 * 
	 * Controller method which creates a brand new Policy. 
	 * 
	 * @param policyRequest
	 * @return
	 */
	@PostMapping("policy/create")
	@ResponseStatus(HttpStatus.CREATED)
	public Policy createPolicy(@RequestBody @ValidPolicyRequest PolicyRequest policyRequest) {

		User requestUser = policyRequest.getUser();
		Asset requestAsset = policyRequest.getAsset();
		PolicyDetails requestPolicyDetails = policyRequest.getPolicyDetails();
		ArrayList<PolicyReference> allUserPolicies;
		ArrayList<AssetReference> allUserAssets = new ArrayList<AssetReference>();

		// Existing User
		if (requestUser.getId() != null) {

			requestUser = userRepository.findOne(requestUser.getId().toString());

			allUserPolicies = requestUser.getPolicyReferences();

			if (requestUser.getAssetReferences() != null) {
				allUserAssets = requestUser.getAssetReferences();
			}
		}
		// New users
		else {
			requestUser = userRepository.save(requestUser);
			allUserPolicies = new ArrayList<PolicyReference>();
		}

		// Check if appending policy to existing User's asset
		// Can assume the Asset is a Valid user's asset. (the Validation logic checks
		// that the input assetId is a valid asset for the User)
		if (requestAsset.getId() != null) {
			requestAsset = assetRepository.findOne(requestAsset.getId().toString());
		}

		// New Asset for the User
		else {
			requestAsset = assetRepository.save(requestAsset);
			// Add the asset to user assets
			AssetReference aRef = new AssetReference().setAssetId(requestAsset.getId().toString())
					.setAssetName(requestAsset.getName());
			allUserAssets.add(aRef);
			requestUser.setAssetReferences(allUserAssets);
		}

		// Define the Policy name (can be something better the simply concatenate IDs)
		String policyName = "POLICY_" + requestUser.getId().toString() + "_" + requestAsset.getId().toString();

		// Create the Policy
		Policy policy = new Policy().setPolicyDetails(requestPolicyDetails).setUserId(requestUser.getId().toString())
				.setAssetId(requestAsset.getId().toString()).setName(policyName).setSuspended(false)
				.setChangeHistory(addChangeLog(new ArrayList<ChangeLog>(), messageSource.getMessage("changelog.policy.created", null, null), LocalDate.now()));

		// Persist the policy into DB
		Policy newPolicy = policyRepository.save(policy);

		// New policy ref to add into the User document
		PolicyReference polRef = new PolicyReference().setPolicyId(newPolicy.getId().toString())
				.setPolicyType(newPolicy.getPolicyDetails().getPolicyType());
		allUserPolicies.add(polRef);
		
		requestUser.setPolicyReferences(allUserPolicies);

		// Update the User (will update policy refs, and asset refs, the policyId, )
		requestUser = userRepository.update(requestUser);
		
		// Return the policy 
		return newPolicy;
	}

	/**
	 * 
	 * Method for Updating a Policy's User Details
	 * 
	 * 
	 * @param user - data that will be updated for the user in the Policy.
	 * @param id   - policy id
	 * @return
	 */
	@PutMapping("policy/{id}/update/user")
	public Policy updatePolicy(@RequestBody @Valid User user, @PathVariable String id) {

		// Loads policy or fails if cannot obtain it
		Policy policy = loadPolicyIdOrFail(id);

		// Policies cannot be updated unless they have payment(s) on them
		verifyPolicyBillingOrFail(id, policy);

		// Only able to modify ESTR policies (not RCOs)
		verifyNonRCOPolicyOrFail(policy);

		String userId = policy.getUserId();
		User updatingUser = userRepository.findOne(userId);

		if (user.getFirstName() != null && !user.getFirstName().equalsIgnoreCase(updatingUser.getFirstName()))
			updatingUser.setFirstName(user.getFirstName());
		if (user.getLastName() != null && !user.getLastName().equalsIgnoreCase(updatingUser.getLastName()))
			updatingUser.setLastName(user.getLastName());
		if (user.getEmailAddress() != null && !user.getEmailAddress().equalsIgnoreCase(updatingUser.getEmailAddress()))
			updatingUser.setEmailAddress(user.getEmailAddress());
		if (user.getIdNumber() != null && !user.getIdNumber().equalsIgnoreCase(updatingUser.getIdNumber()))
			updatingUser.setIdNumber(user.getIdNumber());
		
		// Update the User 
		updatingUser = userRepository.update(updatingUser);

		// History -Change log entry
		policy.setChangeHistory(addChangeLog(policy.getChangeHistory(), messageSource.getMessage("changelog.policy.user.update", null, null), LocalDate.now()));
		
		// Update and return the updated policy
		return policyRepository.update(policy);
	}

	/**
	 * 
	 * Controller method which handles updates of a policy's asset
	 * 
	 * 
	 * @param asset - data that will be updated for the asset in the Policy.
	 * @param id   - policy id
	 * @return
	 */
	@PutMapping("policy/{id}/update/asset")
	public Policy updatePolicy(@RequestBody @Valid Asset asset, @PathVariable String id) {

		// Loads policy or fails if cannot obtain it
		Policy policy = loadPolicyIdOrFail(id);

		// Policies cannot be updated unless they have payment(s) on them
		verifyPolicyBillingOrFail(id, policy);

		// Only able to modify ESTR policies (not RCOs)
		verifyNonRCOPolicyOrFail(policy);

		// Lookup the Asset
		Asset updatingAsset = assetRepository.findOne(policy.getAssetId());

		// track changes to the name...(if the name changes, then have to update the
		// User's Asset list.. see below)
		boolean nameChanging = false;
		if (asset.getName() != null && !asset.getName().equalsIgnoreCase(updatingAsset.getName())) {
			updatingAsset.setName(asset.getName());
			nameChanging = true;
		}

		// Update the ASSET fields that are getting changed.
		if (asset.getModel() != null && !asset.getModel().equalsIgnoreCase(updatingAsset.getModel())) {
			updatingAsset.setModel(asset.getModel());
		}
		if (asset.getMake() != null && !asset.getMake().equalsIgnoreCase(updatingAsset.getMake())) {
			updatingAsset.setMake(asset.getMake());
		}
		if (asset.getDescription() != null
				&& !asset.getDescription().equalsIgnoreCase(updatingAsset.getDescription())) {
			updatingAsset.setDescription(asset.getDescription());
		}
		if (asset.getMaxSpeedKmH() != null && !(asset.getMaxSpeedKmH() == updatingAsset.getMaxSpeedKmH())) {
			updatingAsset.setMaxSpeedKmH(asset.getMaxSpeedKmH());
		}

		// Update the Asset
		updatingAsset = assetRepository.update(updatingAsset);

		// If the Name changed, then have to update the User's Asset list (as it
		// uses/displays the Asset's name)
		if (nameChanging) {
			User updatingUser = userRepository.findOne(policy.getUserId());
			if (updatingUser.getAssetReferences() != null) {
				for (AssetReference aRef : updatingUser.getAssetReferences()) {
					if (aRef.getAssetId().equalsIgnoreCase(updatingAsset.getId().toString())) {
						aRef.setAssetName(updatingAsset.getName());

					}
				}
			}
			// Update User to have their latest asset list.
			userRepository.update(updatingUser);
		}

		// History - Change log entry
		policy.setChangeHistory(addChangeLog(policy.getChangeHistory(), messageSource.getMessage("changelog.policy.asset.update", null, null), LocalDate.now()));
		
		// Update and return the updated policy
		return policyRepository.update(policy);
	}

	/**
	 * 
	 * Controller method which handles update of a policy details. 
	 * 
	 * 
	 * @param policyDetails - data that will be updated for the policy-details in the Policy.
	 * @param id   - policy id
	 * @return
	 */
	@PutMapping("policy/{id}/update/policy-details")
	public Policy updatePolicy(@RequestBody @Valid PolicyDetails policyDetails, @PathVariable String id) {

		// Loads policy or fails if cannot obtain it
		Policy policy = loadPolicyIdOrFail(id);

		// Policies cannot be updated unless they have payment(s) on them
		verifyPolicyBillingOrFail(id, policy);

		// Only able to modify ESTR policies (not RCOs)
		verifyNonRCOPolicyOrFail(policy);
		
		// If it gets here, its surely an EST Policy its trying to update (which always contains a mandatory DT at very least). 
		ArrayList<CoverType> coverTypes = verifyESTPolicyCoverTypesOrFail(id, policyDetails.getCoverTypes());

		// Set updated policy details
		policyDetails.setCoverTypes(coverTypes);
		policy.setPolicyDetails(policyDetails);

		// History - Change log entry
		policy.setChangeHistory(addChangeLog(policy.getChangeHistory(), messageSource.getMessage("changelog.policy.details.update", null, null), LocalDate.now()));

		// Update and return the updated policy
		return policyRepository.update(policy);
	}


	/**
	 * 
	 * 
	 * Controller Method which handles payment of a policy. 
	 * 
	 * 
	 * @param id - the policy ID
	 * @return
	 */
	@PutMapping("policy/{id}/makePayment")
	public Policy makePayment(@PathVariable String id) {

		// Loads policy or fails if cannot obtain it
		Policy policy = loadPolicyIdOrFail(id);

		// Load relevant asset
		Asset asset = assetRepository.findOne(policy.getAssetId().toString());

		// Get list of registered policies for the Asset
		ArrayList<PolicyReference> activeExistingPolicies = asset.getActivePoliciesReferences();

		// Check if existing/active policy of same type (fail if so)
		if (activeExistingPolicies != null) {
			for (PolicyReference pRef : activeExistingPolicies) {
				if (pRef.getPolicyType().equals(policy.getPolicyDetails().getPolicyType())) {
					throw new CustomException(messageSource.getMessage("error.policy.cannot.pay", null, null));
				}
			}
		} else {
			activeExistingPolicies = new ArrayList<PolicyReference>();
		}
		
		// Add active policy to asset
		activeExistingPolicies.add(new PolicyReference()
				.setPolicyId(id)
				.setPolicyType(policy.getPolicyDetails().getPolicyType()));
		asset.setActivePoliciesReferences(activeExistingPolicies);
		asset = assetRepository.update(asset);

		// Add billing entry
		PaymentDetails payment = new PaymentDetails();
		payment.setCreationDate(LocalDate.now());
		payment.setStartDate(LocalDate.now());
		payment.setEndDate(LocalDate.now()
				.plusMonths(PolicyType.POLICY_DURATIONS_BY_TYPE.get(policy.getPolicyDetails().getPolicyType())));
		ArrayList<PaymentDetails> billingList = new ArrayList<PaymentDetails>();
		billingList.add(payment);
		policy.setBilling(billingList);

		// History - Change log entry
		policy.setChangeHistory(addChangeLog(policy.getChangeHistory(), messageSource.getMessage("changelog.policy.payment", null, null), LocalDate.now()));

		// Update and return updated policy
		return policyRepository.update(policy);
	}

	/**
	 * 
	 * Controller method which handles suspension of a policy.
	 * 
	 * 
	 * @param id - the policy ID
	 * @return
	 */
	@PutMapping("policy/{id}/suspend")
	public Policy suspendPolicy(@PathVariable String id) {

		// Loads policy or fails if cannot obtain it
		Policy policy = loadPolicyIdOrFail(id);

		// Policies cannot be suspended unless they have payment(s) on them
		verifyPolicyBillingOrFail(id, policy);
		
		// Policies cannot be suspended if they have already been suspended previously
		verifyPolicyNotSuspendedOrFail(policy);

		ArrayList<PaymentDetails> payments = policy.getBilling();
		// Get the latest payment
		PaymentDetails payDetails = payments.get(payments.size() - 1);
		payDetails.setEndDate(LocalDate.now());
		payments.remove(payments.size() - 1);
		payments.add(payDetails);
		policy.setBilling(payments);
		policy.setSuspended(true);

		Asset updatingAsset = assetRepository.findOne(policy.getAssetId());

		ArrayList<PolicyReference> activeAssetPolicies = updatingAsset.getActivePoliciesReferences();
		for (int i = 0; i < activeAssetPolicies.size(); i++) {
			if (activeAssetPolicies.get(i).getPolicyId().equalsIgnoreCase(policy.getId().toString())) {

				activeAssetPolicies.remove(i);
			}
		}
		updatingAsset.setActivePoliciesReferences(activeAssetPolicies);
		updatingAsset = assetRepository.update(updatingAsset);

		// History - Change log entry
		policy.setChangeHistory(addChangeLog(policy.getChangeHistory(), messageSource.getMessage("changelog.policy.suspension", null, null), LocalDate.now()));

		// Update and return updated policy 
		return policyRepository.update(policy);
	}

	// Method used when trying to suspend a policy. Will fail if policy was previously suspended already. 
	private void verifyPolicyNotSuspendedOrFail(Policy policy) {
		if (policy.isSuspended()) {
			String errorMessage = messageSource.getMessage("error.policy.cannot.resuspend", new Object[] {policy.getId().toString()}, null);
			throw new CustomException(errorMessage);
		}
	}

	// Method used when updating a policy. Will fail if trying to update a RCO type policy 
	private void verifyNonRCOPolicyOrFail(Policy policy) {
		if (policy.getPolicyDetails().getPolicyType() == PolicyType.RCO) {
			String errorMessage = messageSource.getMessage("error.policy.rco.cannot.modify", new Object[] {policy.getId().toString()}, null);
			throw new CustomException(errorMessage);
		}
	}

	// Method used when updating a policy. Will fail if policy has no billing records on it
	private void verifyPolicyBillingOrFail(String id, Policy policy) {
		if (policy.getBilling() == null) {
			String errorMessage = messageSource.getMessage("error.policy.update.unpaid", new Object[] {id}, null);
			throw new CustomException(errorMessage);
		}
	}

	// Used to try and load a policy given an "id". Will fail if id is "bad" or cannot find policy in db 
	private Policy loadPolicyIdOrFail(String id) {
		if (!ObjectId.isValid(id)) {
			String errorMessage = messageSource.getMessage("error.policy.invalid.id", new Object[] {id}, null);
			throw new CustomException(errorMessage);
		}
		Policy policy = policyRepository.findOne(id);
		if (policy == null) {
			String errorMessage = messageSource.getMessage("error.policy.not.exist", new Object[] {id}, null);
			throw new CustomException(errorMessage);
		}
		return policy;
	}

	// Utility method that builds a change-log entry 
	private ArrayList<ChangeLog> addChangeLog(ArrayList<ChangeLog> changeHistory, String changeHistoryMessage, LocalDate time) {
		changeHistory.add(new ChangeLog().setDateChanged(time).setComment(changeHistoryMessage));
		return changeHistory;
	}
	
	// Utility method that validates properly passed in EST Cover Types during a policy update. 
	private ArrayList<CoverType> verifyESTPolicyCoverTypesOrFail(String pId, ArrayList<CoverType> coverTypes) {

		if (coverTypes.contains(CoverType.DT)) {
			throw new CustomException(messageSource.getMessage("error.policy.update.est.dup.dt", new Object[] {pId}, null));
		}
		
		if (coverTypes.size() > 2  || coverTypes.size() == 0) {
			throw new CustomException(messageSource.getMessage("error.policy.update.est.bad.values", new Object[] {pId}, null));
		}
		
		// Ensure mandatory DT is always there for updates. 
		coverTypes.add(CoverType.DT);
		return coverTypes;
	}
}
