package com.mati.insu.policies.controllers;

import javax.validation.constraints.NotNull;
import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.mati.insu.exception.CustomException;
import com.mati.insu.policies.models.User;
import com.mati.insu.policies.repositories.UserRepository;

@Validated
@RestController
@RequestMapping("/api")
public class UserController {

	private final UserRepository userRepository;

	private static final String INVALID_ID_MESSAGE = "Invalid ID %s was provided, try with a correctly formatted ID";
	
	public UserController(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@PostMapping("user")
	@ResponseStatus(HttpStatus.CREATED)
	public User postUser(@RequestBody @NotNull User user) {
		return userRepository.save(user);
	}

	@DeleteMapping("user/{id}")
	public Long deleteUser(@PathVariable String id) {
    	if (!ObjectId.isValid(id)) {
    		throw new CustomException(String.format(INVALID_ID_MESSAGE, id));
    	}
		return userRepository.delete(id);
	}

	@GetMapping("user/{id}")
	public User getUser(@PathVariable String id) {
    	if (!ObjectId.isValid(id)) {
    		throw new CustomException(String.format(INVALID_ID_MESSAGE, id));
    	}
		User user = userRepository.findOne(id);
		if (user == null) {
			throw new CustomException(String.format("No User with ID %s was found in DB, try a different Id", id));
		}
		return user;
	}

}
