package com.mati.insu.policies.controllers;

import org.bson.types.ObjectId;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mati.insu.exception.CustomException;
import com.mati.insu.policies.models.Asset;
import com.mati.insu.policies.repositories.AssetRepository;

@Validated
@RestController
@RequestMapping("/api")
public class AssetController {
	    
	private final AssetRepository assetRepository;
	
    private static final String INVALID_ID_MESSAGE = "Invalid ID %s was provided, try with a correctly formatted ID";

    public AssetController(AssetRepository assetRepository) {
        this.assetRepository = assetRepository; 
    }
    
    @PostMapping("asset")
    @ResponseStatus(HttpStatus.CREATED)
    public Asset postAsset(@RequestBody Asset asset) {
        return assetRepository.save(asset);
    }

    @DeleteMapping("asset/{id}")
    public Long deleteAsset(@PathVariable String id) {
    	if (!ObjectId.isValid(id)) {
    		throw new CustomException(String.format(INVALID_ID_MESSAGE, id));
    	}
        return assetRepository.delete(id);
    }

    @GetMapping("asset/{id}")
    public Asset getAsset(@PathVariable String id) {
    	if (!ObjectId.isValid(id)) {
    		throw new CustomException(String.format(INVALID_ID_MESSAGE, id));
    	}
    	Asset asset = assetRepository.findOne(id);
		if (asset == null) {
			throw new CustomException(String.format("No Asset with ID %s was found in DB, try a different Id", id));
		}		
		return asset;
    }
}
