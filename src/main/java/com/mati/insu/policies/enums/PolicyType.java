package com.mati.insu.policies.enums;

import java.util.HashMap;

public enum PolicyType {
	
	EST,
	RCO;
	
    public final static HashMap<PolicyType, Integer> POLICY_DURATIONS_BY_TYPE = new HashMap<PolicyType, Integer>();
    
    static {
    	POLICY_DURATIONS_BY_TYPE.put(EST,  6);
    	POLICY_DURATIONS_BY_TYPE.put(RCO,  12);
    }
}
