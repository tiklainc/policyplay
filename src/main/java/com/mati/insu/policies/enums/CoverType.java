package com.mati.insu.policies.enums;

public enum CoverType {

	RT,
	DM,
	DT,
	DTO;

}